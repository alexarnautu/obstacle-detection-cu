import cv2
import numpy as np

from models.BoundBox import BoundBox


def _overlap_intervals(a, b):
    x1, x2 = a
    x3, x4 = b

    if x3 < x1:
        if x4 < x1:
            return 0
        else:
            return min(x2, x4) - x1
    else:
        if x2 < x3:
            return 0
        else:
            return min(x2, x4) - x3


def _compute_sigmoid_function(x):
    return 1. / (1. + np.exp(-x))


def _compute_softmax_function(x, axis=-1, t=-100.):
    x = x - np.max(x)

    if np.min(x) < t:
        x = x / np.min(x) * t

    e_x = np.exp(x)

    return e_x / e_x.sum(axis, keepdims=True)


def compute_overlap(a, b):
    """
    Code originally from https://github.com/rbgirshick/py-faster-rcnn.
    Parameters
    ----------
    a: (N, 4) ndarray of float
    b: (K, 4) ndarray of float
    Returns
    -------
    overlaps: (N, K) ndarray of overlap between boxes and query_boxes
    """
    area = (b[:, 2] - b[:, 0]) * (b[:, 3] - b[:, 1])

    iw = np.minimum(np.expand_dims(a[:, 2], axis=1), b[:, 2]) - np.maximum(np.expand_dims(a[:, 0], 1), b[:, 0])
    ih = np.minimum(np.expand_dims(a[:, 3], axis=1), b[:, 3]) - np.maximum(np.expand_dims(a[:, 1], 1), b[:, 1])

    iw = np.maximum(iw, 0)
    ih = np.maximum(ih, 0)

    ua = np.expand_dims((a[:, 2] - a[:, 0]) * (a[:, 3] - a[:, 1]), axis=1) + area - iw * ih

    ua = np.maximum(ua, np.finfo(float).eps)

    intersection = iw * ih

    return intersection / ua


def compute_ap(recall, precision):
    """ Compute the average precision, given the recall and precision curves.
    Code originally from https://github.com/rbgirshick/py-faster-rcnn.

    # Arguments
        recall:    The recall curve (list).
        precision: The precision curve (list).
    # Returns
        The average precision as computed in py-faster-rcnn.
    """
    # correct AP calculation
    # first append sentinel values at the end
    mrec = np.concatenate(([0.], recall, [1.]))
    mpre = np.concatenate(([0.], precision, [0.]))

    # compute the precision envelope
    for i in range(mpre.size - 1, 0, -1):
        mpre[i - 1] = np.maximum(mpre[i - 1], mpre[i])

    # to calculate area under PR curve, look for points
    # where X axis (recall) changes value
    i = np.where(mrec[1:] != mrec[:-1])[0]

    # and sum (\Delta recall) * prec
    ap = np.sum((mrec[i + 1] - mrec[i]) * mpre[i + 1])
    return ap


def draw_boxes_and_labels_on_image(image, boxes, labels):
    """
    Draws the boxes and labels on the image
    """
    image_height, image_width, _ = image.shape

    for box in boxes:
        x_min = int(box.xmin * image_width)
        y_min = int(box.ymin * image_height)
        x_max = int(box.xmax * image_width)
        y_max = int(box.ymax * image_height)

        cv2.rectangle(image, (x_min, y_min), (x_max, y_max), (0, 255, 0), 3)
        cv2.putText(image,
                    labels[box.get_label()] + ' ' + str(box.get_score()),
                    (x_min, y_min - 13),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    1e-3 * image_height,
                    (0, 255, 0), 2)
    return image


def decode_network_output(output, anchors, no_classes, obj_threshold=0.3, nms_threshold=0.3):
    boxes = []
    grid_height, grid_width, no_box = output.shape[:3]

    # Decode the output by the network
    output[..., 4] = _compute_sigmoid_function(output[..., 4])
    output[..., 5:] = output[..., 4][..., np.newaxis] * _compute_softmax_function(output[..., 5:])
    output[..., 5:] *= output[..., 5:] > obj_threshold

    for row in range(grid_height):
        for col in range(grid_width):
            for box_index in range(no_box):
                # from 4th element onwards are confidence and class classes
                classes = output[row, col, box_index, 5:]

                if np.sum(classes) > 0:
                    # first 4 elements are x, y, w, and h
                    x, y, w, h = output[row, col, box_index, :4]

                    x = (col + _compute_sigmoid_function(x)) / grid_width  # center position, unit: image width
                    y = (row + _compute_sigmoid_function(y)) / grid_height  # center position, unit: image height
                    w = anchors[2 * box_index + 0] * np.exp(w) / grid_width  # unit: image width
                    h = anchors[2 * box_index + 1] * np.exp(h) / grid_height  # unit: image height
                    confidence = output[row, col, box_index, 4]

                    box = BoundBox(x - w / 2, y - h / 2, x + w / 2, y + h / 2, confidence, classes)
                    boxes.append(box)
    # suppress non-maximal boxes
    for class_index in range(no_classes):
        sorted_indices = list(reversed(np.argsort([box.classes[class_index] for box in boxes])))

        for i in range(len(sorted_indices)):
            index_i = sorted_indices[i]

            if boxes[index_i].classes[class_index] == 0:
                continue
            else:
                for j in range(i + 1, len(sorted_indices)):
                    index_j = sorted_indices[j]

                    if compute_box_iou(boxes[index_i], boxes[index_j]) >= nms_threshold:
                        boxes[index_j].classes[class_index] = 0
    # remove the boxes which are less likely than a obj_threshold
    boxes = [box for box in boxes if box.get_score() > obj_threshold]
    return boxes


def compute_box_iou(a, b):
    """
    Computes the IOU between two boxes
    :return:
    """
    intersect = _overlap_intervals([a.xmin, a.xmax], [b.xmin, b.xmax]) * _overlap_intervals([a.ymin, a.ymax],
                                                                                            [b.ymin, b.ymax])

    w1, h1 = a.xmax - a.xmin, a.ymax - a.ymin
    w2, h2 = b.xmax - b.xmin, b.ymax - b.ymin

    return float(intersect) / (w1 * h1 + w2 * h2 - intersect)
