import gi
import time
import threading
from datetime import datetime

gi.require_version('Gdk', '3.0')
gi.require_version('Gtk', '3.0')

from gi.repository import Gdk, Gtk, GLib, GObject


class Recorder(Gtk.Window):
    def __init__(self):
        self.label_lock = threading.Lock()

        Gtk.Window.__init__(self, title="Recorder")

        self.set_border_width(10)
        main_layout = Gtk.VBox(spacing=6)
        self.add(main_layout)

        hbox = Gtk.HBox()
        main_layout.add(hbox)

        self.button = Gtk.Button.new_with_label("Start")
        hbox.pack_start(self.button, True, True, 0)
        self.button.connect('clicked', self.start_recording)

        self.label = Gtk.Label(label="Recording not started")
        hbox.pack_start(self.label, True, True, 0)

        vbox = Gtk.VBox()
        main_layout.add(vbox)

        self.image = Gtk.Image.new_from_pixbuf(self.get_pixbuf_from_screen())
        vbox.pack_start(self.image, True, True, 0)

    def start_recording(self, event):
        self.button.set_sensitive(False)
        thread = threading.Thread(target=self.start_final_countdown)
        thread.daemon = True
        thread.start()

    def start_final_countdown(self):
        for index in reversed(range(1, 6)):
            self.label.set_text(f'Recording starting in {index}')
            time.sleep(1)

        self.label.set_text('Recording in progress')

        for index in range(1, 6):
            self.label.set_text(f'Taken {index} images.')

            pb = Recorder.get_pixbuf_from_screen()
            pb.savev(f'training_data_unprocessed/{datetime.now()}.jpg', 'jpeg', ["quality"], ["100"])
            self.image.set_from_pixbuf(pb)
            time.sleep(1)

        self.label.set_text('Recoding stopped.')

        self.button.set_sensitive(True)

    @staticmethod
    def grab_image_and_save_to_disk(self):
        pb = Recorder.get_pixbuf_from_screen()
        # Save the training data.
        pb.savev(f'training_data_unprocessed/{datetime.now()}.jpg', 'jpeg', ["quality"], ["100"])
        # GLib.timeout_add(100, self.grab_image_and_save_to_disk)

    @staticmethod
    def get_pixbuf_from_screen():
        window = Gdk.get_default_root_window()
        pixbuf = Gdk.pixbuf_get_from_window(window, 0, 54, 800, 600)
        return pixbuf


if __name__ == '__main__':
    recorder = Recorder()
    recorder.connect('delete-event', Gtk.main_quit)
    recorder.show_all()

    Gtk.main()
