import json

from constants import *
from models.YOLO import YOLO
from preprocessing import parse_annotation

if __name__ == '__main__':
    with open(CONFIG_FILE) as config_file:
        config = json.loads(config_file.read())

    train_images, train_labels = parse_annotation(TRAIN_ANNOTATIONS_FOLDER, TRAIN_IMAGES_FOLDER,
                                                  config['model']['labels'])

    valid_images, valid_labels = parse_annotation(VALID_ANNOTATIONS_FOLDER, VALID_IMAGES_FOLDER,
                                                  config['model']['labels'])

    yolo = YOLO(input_size=416,
                labels=config['model']['labels'],
                max_box_per_image=10,
                anchors=[0.57273, 0.677385, 1.87446, 2.06253, 3.33843, 5.47434, 7.88282, 3.52778, 9.77052, 9.16828])
    yolo.train(train_images=train_images,
               valid_images=valid_images,
               train_times=config['train']['train_times'],
               valid_times=config['valid']['valid_times'],
               no_epochs=config['train']['no_epochs'],
               learning_rate=config['train']['learning_rate'],
               batch_size=config['train']['batch_size'],
               warmup_epochs=config['train']['warmup_epochs'],
               object_scale=config['train']['object_scale'],
               no_object_scale=config['train']['no_object_scale'],
               coord_scale=config['train']['coord_scale'],
               class_scale=config['train']['class_scale'],
               saved_weights_name=config['train']['saved_weights_name'])