#! /usr/bin/env python

import argparse
import cv2
import json
import numpy as np

from constants import *

from tqdm import tqdm
from utils import draw_boxes_and_labels_on_image
from models.YOLO import YOLO

argparser = argparse.ArgumentParser()

argparser.add_argument(
    '-w',
    '--weights',
    help='path to pretrained weights')

argparser.add_argument(
    '-i',
    '--input',
    help='path to an image or an video (mp4 format)')


def _main_(args):
    weights_path = args.weights
    image_path = args.input

    with open(CONFIG_FILE) as config_buffer:
        config = json.load(config_buffer)

    yolo = YOLO(input_size=416,  labels=config['model']['labels'],
                max_box_per_image=10, anchors=config['model']['anchors'])

    yolo.load_weights(weights_path)

    image = cv2.imread(image_path)
    boxes = yolo.predict(image)
    image = draw_boxes_and_labels_on_image(image, boxes, config['model']['labels'])

    print(len(boxes), 'boxes are found')

    cv2.imwrite(image_path[:-4] + '_detected' + image_path[-4:], image)


if __name__ == '__main__':
    args = argparser.parse_args()
    _main_(args)
