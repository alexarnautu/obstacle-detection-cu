import os
import cv2
import numpy as np
import tensorflow as tf
from keras.callbacks import EarlyStopping, ModelCheckpoint, TensorBoard

from keras.engine import Model

from keras.layers import Input, Conv2D, Reshape, Lambda
from keras.optimizers import Adam

from models.BatchGenerator import BatchGenerator
from models.YOLOFeature import YOLOFeature
from utils import compute_overlap, compute_ap, decode_network_output


class YOLO(object):
    def __init__(self, input_size, labels, max_box_per_image, anchors):
        self.input_size = input_size

        self.labels = list(labels)
        self.no_classes = len(self.labels)
        self.no_box = len(anchors) // 2
        self.class_weights = np.ones(self.no_classes, dtype='float32')
        self.anchors = anchors

        self.max_box_per_image = max_box_per_image

        # Build de model
        input_image = Input(shape=(self.input_size, self.input_size, 3))
        self.true_boxes = Input(shape=(1, 1, 1, max_box_per_image, 4))

        self.feature_extractor = YOLOFeature(self.input_size)
        self.grid_height, self.grid_width = self.feature_extractor.get_output_shape()
        features = self.feature_extractor.extract(input_image)

        output = Conv2D(self.no_box * (4 + 1 + self.no_classes), (1, 1), strides=(1, 1), padding='same',
                        name='DetectionLayer', kernel_initializer='lecun_normal')(features)
        output = Reshape((self.grid_height, self.grid_width, self.no_box, 4 + 1 + self.no_classes))(output)
        output = Lambda(lambda args: args[0])([output, self.true_boxes])

        self.model = Model([input_image, self.true_boxes], output)

        # Initialize the weights of the detection layer
        layer = self.model.layers[-4]
        weights = layer.get_weights()

        new_kernel = np.random.normal(size=weights[0].shape) / (self.grid_height * self.grid_width)
        new_bias = np.random.normal(size=weights[1].shape) / (self.grid_height * self.grid_width)

        layer.set_weights([new_kernel, new_bias])

        # Print a summary of the whole model
        self.model.summary()

    def custom_loss(self, y_true, y_pred):
        mask_shape = tf.shape(y_true)[:4]

        cell_x = tf.to_float(
            tf.reshape(tf.tile(tf.range(self.grid_width), [self.grid_height]),
                       (1, self.grid_height, self.grid_width, 1, 1)))
        cell_y = tf.transpose(cell_x, (0, 2, 1, 3, 4))

        cell_grid = tf.tile(tf.concat([cell_x, cell_y], -1), [self.batch_size, 1, 1, self.no_box, 1])

        coord_mask = tf.zeros(mask_shape)
        conf_mask = tf.zeros(mask_shape)
        class_mask = tf.zeros(mask_shape)

        seen = tf.Variable(0.)
        total_recall = tf.Variable(0.)

        """
        Adjust prediction
        """
        pred_box_xy = tf.sigmoid(y_pred[..., :2]) + cell_grid
        pred_box_wh = tf.exp(y_pred[..., 2:4]) * np.reshape(self.anchors, [1, 1, 1, self.no_box, 2])
        pred_box_conf = tf.sigmoid(y_pred[..., 4])
        pred_box_class = y_pred[..., 5:]

        """
        Adjust ground truth
        """
        true_box_xy = y_true[..., 0:2]
        true_box_wh = y_true[..., 2:4]

        true_wh_half = true_box_wh / 2.
        true_mins = true_box_xy - true_wh_half
        true_maxes = true_box_xy + true_wh_half

        pred_wh_half = pred_box_wh / 2.
        pred_mins = pred_box_xy - pred_wh_half
        pred_maxes = pred_box_xy + pred_wh_half

        intersect_mins = tf.maximum(pred_mins, true_mins)
        intersect_maxes = tf.minimum(pred_maxes, true_maxes)
        intersect_wh = tf.maximum(intersect_maxes - intersect_mins, 0.)
        intersect_areas = intersect_wh[..., 0] * intersect_wh[..., 1]

        true_areas = true_box_wh[..., 0] * true_box_wh[..., 1]
        pred_areas = pred_box_wh[..., 0] * pred_box_wh[..., 1]

        union_areas = pred_areas + true_areas - intersect_areas
        iou_scores = tf.truediv(intersect_areas, union_areas)

        true_box_conf = iou_scores * y_true[..., 4]

        true_box_class = tf.argmax(y_true[..., 5:], -1)
        """
        Determine the masks
        """
        coord_mask = tf.expand_dims(y_true[..., 4], axis=-1) * self.coord_scale

        true_xy = self.true_boxes[..., 0:2]
        true_wh = self.true_boxes[..., 2:4]

        true_wh_half = true_wh / 2.
        true_mins = true_xy - true_wh_half
        true_maxes = true_xy + true_wh_half

        pred_xy = tf.expand_dims(pred_box_xy, 4)
        pred_wh = tf.expand_dims(pred_box_wh, 4)

        pred_wh_half = pred_wh / 2.
        pred_mins = pred_xy - pred_wh_half
        pred_maxes = pred_xy + pred_wh_half

        intersect_mins = tf.maximum(pred_mins, true_mins)
        intersect_maxes = tf.minimum(pred_maxes, true_maxes)
        intersect_wh = tf.maximum(intersect_maxes - intersect_mins, 0.)
        intersect_areas = intersect_wh[..., 0] * intersect_wh[..., 1]

        true_areas = true_wh[..., 0] * true_wh[..., 1]
        pred_areas = pred_wh[..., 0] * pred_wh[..., 1]

        union_areas = pred_areas + true_areas - intersect_areas
        iou_scores = tf.truediv(intersect_areas, union_areas)

        best_ious = tf.reduce_max(iou_scores, axis=4)
        conf_mask = conf_mask + tf.to_float(best_ious < 0.6) * (1 - y_true[..., 4]) * self.no_object_scale

        # penalize the confidence of the boxes, which are reponsible for corresponding ground truth box
        conf_mask = conf_mask + y_true[..., 4] * self.object_scale

        ### class mask: simply the position of the ground truth boxes (the predictors)
        class_mask = y_true[..., 4] * tf.gather(self.class_weights, true_box_class) * self.class_scale

        """
        Warm-up training
        """
        no_boxes_mask = tf.to_float(coord_mask < self.coord_scale / 2.)
        seen = tf.assign_add(seen, 1.)

        true_box_xy, true_box_wh, coord_mask = tf.cond(tf.less(seen, self.warmup_batches + 1),
                                                       lambda: [true_box_xy + (0.5 + cell_grid) * no_boxes_mask,
                                                                true_box_wh + tf.ones_like(true_box_wh) * \
                                                                np.reshape(self.anchors, [1, 1, 1, self.no_box, 2]) * \
                                                                no_boxes_mask,
                                                                tf.ones_like(coord_mask)],
                                                       lambda: [true_box_xy,
                                                                true_box_wh,
                                                                coord_mask])

        """
        Finalize the loss
        """
        no_coord_boxes = tf.reduce_sum(tf.to_float(coord_mask > 0.0))
        no_conf_boxes = tf.reduce_sum(tf.to_float(conf_mask > 0.0))
        no_class_boxes = tf.reduce_sum(tf.to_float(class_mask > 0.0))

        loss_xy = tf.reduce_sum(tf.square(true_box_xy - pred_box_xy) * coord_mask) / (no_coord_boxes + 1e-6) / 2.
        loss_wh = tf.reduce_sum(tf.square(true_box_wh - pred_box_wh) * coord_mask) / (no_coord_boxes + 1e-6) / 2.
        loss_conf = tf.reduce_sum(tf.square(true_box_conf - pred_box_conf) * conf_mask) / (no_conf_boxes + 1e-6) / 2.
        loss_class = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=true_box_class, logits=pred_box_class)
        loss_class = tf.reduce_sum(loss_class * class_mask) / (no_class_boxes + 1e-6)

        loss = tf.cond(tf.less(seen, self.warmup_batches + 1),
                       lambda: loss_xy + loss_wh + loss_conf + loss_class + 10,
                       lambda: loss_xy + loss_wh + loss_conf + loss_class)

        nb_true_box = tf.reduce_sum(y_true[..., 4])
        nb_pred_box = tf.reduce_sum(tf.to_float(true_box_conf > 0.5) * tf.to_float(pred_box_conf > 0.3))

        current_recall = nb_pred_box / (nb_true_box + 1e-6)
        total_recall = tf.assign_add(total_recall, current_recall)

        loss = tf.Print(loss, [loss_xy], message='Loss XY \t', summarize=1000)
        loss = tf.Print(loss, [loss_wh], message='Loss WH \t', summarize=1000)
        loss = tf.Print(loss, [loss_conf], message='Loss Conf \t', summarize=1000)
        loss = tf.Print(loss, [loss_class], message='Loss Class \t', summarize=1000)
        loss = tf.Print(loss, [loss], message='Total Loss \t', summarize=1000)
        loss = tf.Print(loss, [current_recall], message='Current Recall \t', summarize=1000)
        loss = tf.Print(loss, [total_recall / seen], message='Average Recall \t', summarize=1000)

        return loss

    def load_weights(self, weights_path):
        self.model.load_weights(weights_path)

    def train(self, train_images, valid_images, train_times, valid_times, no_epochs,
              learning_rate, batch_size, warmup_epochs, object_scale, no_object_scale,
              coord_scale, class_scale, saved_weights_name='best_weights.h5'):
        self.batch_size = batch_size
        self.object_scale = object_scale
        self.no_object_scale = no_object_scale
        self.coord_scale = coord_scale
        self.class_scale = class_scale

        generator_config = {
            'IMAGE_H': self.input_size,
            'IMAGE_W': self.input_size,
            'GRID_H': self.grid_height,
            'GRID_W': self.grid_width,
            'BOX': self.no_box,
            'LABELS': self.labels,
            'CLASS': len(self.labels),
            'ANCHORS': self.anchors,
            'BATCH_SIZE': self.batch_size,
            'TRUE_BOX_BUFFER': self.max_box_per_image
        }
        train_generator = BatchGenerator(train_images, generator_config, norm=self.feature_extractor.normalize)
        valid_generator = BatchGenerator(valid_images, generator_config, norm=self.feature_extractor.normalize,
                                         jitter=False)

        self.warmup_batches = warmup_epochs * (train_times * len(train_generator) + valid_times * len(valid_generator))

        # Compile de model
        optimizer = Adam(lr=learning_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0)
        self.model.compile(loss=self.custom_loss, optimizer=optimizer)

        early_stop = EarlyStopping(monitor='val_loss', min_delta=0.0001, patience=3, mode='min', verbose=1)
        checkpoint = ModelCheckpoint(saved_weights_name, monitor='val_loss', verbose=1, save_best_only=True,
                                     mode='min', period=1)
        tensorboard = TensorBoard(log_dir=os.path.expanduser('logs/'), histogram_freq=0, write_graph=True,
                                  write_images=False)

        # Train
        self.model.fit_generator(generator=train_generator,
                                 steps_per_epoch=len(train_generator) * train_times,
                                 epochs=warmup_epochs + no_epochs,
                                 verbose=2,
                                 validation_data=valid_generator,
                                 validation_steps=len(valid_generator) * valid_times,
                                 callbacks=[early_stop, checkpoint, tensorboard],
                                 workers=3,
                                 max_queue_size=8)

        average_precisions = self.evaluate(valid_generator)

        for label, average_precision in average_precisions.items():
            print(self.labels[label], '{:.4f}'.format(average_precision))
        print('mAP: {:.4f}'.format(sum(average_precisions.values()) / len(average_precisions)))

    def evaluate(self, generator, iou_threshold=0.3):
        all_detections = [[None for _ in range(generator.num_classes())] for _ in range(generator.size())]
        all_annotations = [[None for _ in range(generator.num_classes())] for _ in range(generator.size())]

        for index in range(generator.size()):
            raw_image = generator.load_image(index)
            raw_height, raw_width, raw_channels = raw_image.shape

            predicted_boxes = self.predict(raw_image)

            score = np.array([box.score for box in predicted_boxes])
            predicted_labels = np.array([box.label for box in predicted_boxes])

            if len(predicted_boxes):
                predicted_boxes = np.array([[box.xmin * raw_width, box.ymin * raw_height, box.xmax * raw_width,
                                             box.ymax * raw_height, box.score] for box in predicted_boxes])
            else:
                predicted_boxes = np.array([[]])

            # sort the boxes and the labels according to scores
            score_sort = np.argsort(-score)
            predicted_labels = predicted_labels[score_sort]
            predicted_boxes = predicted_boxes[score_sort]

            # copy detections to all_detections
            for label in range(generator.num_classes()):
                all_detections[index][label] = predicted_boxes[predicted_labels == label, :]

            annotations = generator.load_annotation(index)

            # copy detections to all_annotations
            for label in range(generator.num_classes()):
                all_annotations[index][label] = annotations[annotations[:, 4] == label, :4].copy()

        # compute mAP by comparing all detections and all annotations
        average_precisions = {}

        for label in range(generator.num_classes()):
            false_positives = np.zeros((0,))
            true_positives = np.zeros((0,))
            scores = np.zeros((0,))
            num_annotations = 0.0

            for index in range(generator.size()):
                num_annotations += annotations.shape[0]
                annotations = all_annotations[index][label]

                detections = all_detections[index][label]
                detected_annotations = []

                for detection in detections:
                    scores = np.append(scores, detection[4])

                    if annotations.shape[0] == 0:
                        false_positives = np.append(false_positives, 1)
                        true_positives = np.append(true_positives, 0)
                        continue

                    overlaps = compute_overlap(np.expand_dims(detection, axis=0), annotations)
                    assigned_annotation = np.argmax(overlaps, axis=1)
                    max_overlap = overlaps[0, assigned_annotation]

                    if max_overlap >= iou_threshold and assigned_annotation not in detected_annotations:
                        false_positives = np.append(false_positives, 0)
                        true_positives = np.append(true_positives, 1)
                        detected_annotations.append(assigned_annotation)
                    else:
                        false_positives = np.append(false_positives, 1)
                        true_positives = np.append(true_positives, 0)

            # no annotations -> AP for this class is 0 (is this correct?)
            if num_annotations == 0:
                average_precisions[label] = 0
                continue

            # sort by score
            indices = np.argsort(-scores)
            false_positives = false_positives[indices]
            true_positives = true_positives[indices]

            # compute false positives and true positives
            false_positives = np.cumsum(false_positives)
            true_positives = np.cumsum(true_positives)

            # compute recall and precision
            recall = true_positives / num_annotations
            precision = true_positives / np.maximum(true_positives + false_positives, np.finfo(np.float64).eps)

            # compute average precision
            average_precision = compute_ap(recall, precision)
            average_precisions[label] = average_precision

        return average_precisions

    def predict(self, image):
        image = self.feature_extractor.normalize(cv2.resize(image, (self.input_size, self.input_size)))

        input_image = np.expand_dims(image[:, :, ::-1], 0)
        dummy_array = np.zeros((1, 1, 1, 1, self.max_box_per_image, 4))

        network_output = self.model.predict([input_image, dummy_array])[0]

        return decode_network_output(network_output, self.anchors, self.no_classes)
