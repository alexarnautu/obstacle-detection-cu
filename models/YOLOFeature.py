import tensorflow as tf

from keras.engine import Model
from keras.layers import Input, Conv2D, BatchNormalization, LeakyReLU, MaxPooling2D, Lambda, concatenate

from constants import YOLO_WEIGHTS_PATH


class YOLOFeature:
    def __init__(self, input_size):
        input_image = Input(shape=(input_size, input_size, 3))

        def space_to_depth_x2(x):
            return tf.space_to_depth(x, block_size=2)

        # Layer 1
        layer = Conv2D(32, (3, 3), strides=(1, 1), padding='same', name='conv_1', use_bias=False)(input_image)
        layer = BatchNormalization(name='norm_1')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)
        layer = MaxPooling2D(pool_size=(2, 2))(layer)

        # Layer 2
        layer = Conv2D(64, (3, 3), strides=(1, 1), padding='same', name='conv_2', use_bias=False)(layer)
        layer = BatchNormalization(name='norm_2')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)
        layer = MaxPooling2D(pool_size=(2, 2))(layer)

        # Layer 3
        layer = Conv2D(128, (3, 3), strides=(1, 1), padding='same', name='conv_3', use_bias=False)(layer)
        layer = BatchNormalization(name='norm_3')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)

        # Layer 4
        layer = Conv2D(64, (1, 1), strides=(1, 1), padding='same', name='conv_4', use_bias=False)(layer)
        layer = BatchNormalization(name='norm_4')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)

        # Layer 5
        layer = Conv2D(128, (3, 3), strides=(1, 1), padding='same', name='conv_5', use_bias=False)(layer)
        layer = BatchNormalization(name='norm_5')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)
        layer = MaxPooling2D(pool_size=(2, 2))(layer)

        # Layer 6
        layer = Conv2D(256, (3, 3), strides=(1, 1), padding='same', name='conv_6', use_bias=False)(layer)
        layer = BatchNormalization(name='norm_6')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)

        # Layer 7
        layer = Conv2D(128, (1, 1), strides=(1, 1), padding='same', name='conv_7', use_bias=False)(layer)
        layer = BatchNormalization(name='norm_7')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)

        # Layer 8
        layer = Conv2D(256, (3, 3), strides=(1, 1), padding='same', name='conv_8', use_bias=False)(layer)
        layer = BatchNormalization(name='norm_8')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)
        layer = MaxPooling2D(pool_size=(2, 2))(layer)

        # Layer 9
        layer = Conv2D(512, (3, 3), strides=(1, 1), padding='same', name='conv_9', use_bias=False)(layer)
        layer = BatchNormalization(name='norm_9')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)

        # Layer 10
        layer = Conv2D(256, (1, 1), strides=(1, 1), padding='same', name='conv_10', use_bias=False)(layer)
        layer = BatchNormalization(name='norm_10')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)

        # Layer 11
        layer = Conv2D(512, (3, 3), strides=(1, 1), padding='same', name='conv_11', use_bias=False)(layer)
        layer = BatchNormalization(name='norm_11')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)

        # Layer 12
        layer = Conv2D(256, (1, 1), strides=(1, 1), padding='same', name='conv_12', use_bias=False)(layer)
        layer = BatchNormalization(name='norm_12')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)

        # Layer 13
        layer = Conv2D(512, (3, 3), strides=(1, 1), padding='same', name='conv_13', use_bias=False)(layer)
        layer = BatchNormalization(name='norm_13')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)

        skip_connection = layer

        layer = MaxPooling2D(pool_size=(2, 2))(layer)

        # Layer 14
        layer = Conv2D(1024, (3, 3), strides=(1, 1), padding='same', name='conv_14', use_bias=False)(layer)
        layer = BatchNormalization(name='norm_14')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)

        # Layer 15
        layer = Conv2D(512, (1, 1), strides=(1, 1), padding='same', name='conv_15', use_bias=False)(layer)
        layer = BatchNormalization(name='norm_15')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)

        # Layer 16
        layer = Conv2D(1024, (3, 3), strides=(1, 1), padding='same', name='conv_16', use_bias=False)(layer)
        layer = BatchNormalization(name='norm_16')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)

        # Layer 17
        layer = Conv2D(512, (1, 1), strides=(1, 1), padding='same', name='conv_17', use_bias=False)(layer)
        layer = BatchNormalization(name='norm_17')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)

        # Layer 18
        layer = Conv2D(1024, (3, 3), strides=(1, 1), padding='same', name='conv_18', use_bias=False)(layer)
        layer = BatchNormalization(name='norm_18')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)

        # Layer 19
        layer = Conv2D(1024, (3, 3), strides=(1, 1), padding='same', name='conv_19', use_bias=False)(layer)
        layer = BatchNormalization(name='norm_19')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)

        # Layer 19
        layer = Conv2D(1024, (3, 3), strides=(1, 1), padding='same', name='conv_20', use_bias=False)(layer)
        layer = BatchNormalization(name='norm_20')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)

        # Layer 21
        skip_connection = Conv2D(64, (1, 1), strides=(1, 1), padding='same', name='conv_21', use_bias=False)(
            skip_connection)
        skip_connection = BatchNormalization(name='norm_21')(skip_connection)
        skip_connection = LeakyReLU(alpha=0.1)(skip_connection)
        skip_connection = Lambda(space_to_depth_x2)(skip_connection)

        layer = concatenate([skip_connection, layer])
        # Layer 22
        layer = Conv2D(1024, (3, 3), strides=(1, 1), padding='same', name='conv_22', use_bias=False)(layer)
        layer = BatchNormalization(name='norm_22')(layer)
        layer = LeakyReLU(alpha=0.1)(layer)

        self.feature_extractor = Model(input_image, layer)
        self.feature_extractor.load_weights(YOLO_WEIGHTS_PATH)

    def normalize(self, image):
        return image / 255.

    def get_output_shape(self):
        return self.feature_extractor.get_output_shape_at(-1)[1:3]

    def extract(self, input_image):
        return self.feature_extractor(input_image)
